import { Component, OnInit } from '@angular/core';
import { UserAccountService } from '../service/data/user-account.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';


@Component({
  selector: 'app-withdraw',
  templateUrl: './withdraw.component.html',
  styleUrls: ['./withdraw.component.css']
})
export class WithdrawComponent implements OnInit {
  account: any = {};
 account_num: any;
 message;
 errorMsg;
 isValid = false;

  constructor(private service: UserAccountService, private router: Router, private activeRoute: ActivatedRoute) { }
  withdrawForm: FormGroup;

  ngOnInit() {
    this.account_num = this.activeRoute.snapshot.params['account_num'];

    this.service.getAccount(this.account_num)
      .subscribe(
        response => {
          this.account = response;
        }
      );

      this.withdrawForm = new FormGroup({
        'amount' : new FormControl(null, Validators.required)
      })
  }

  handleWithdraw() {
    let amount = this.withdrawForm.value.amount;
    let client_account = {'display_balance' : amount};

    let updated;

    this.service.withdraw(this.account_num, client_account).subscribe((response =>{
      updated = response;

      if(updated > 0){
        this.message = "Successful Transaction";
        this.isValid = true;

        setTimeout(() => {
          this.router.navigate(['transactional']);
        }, 5000);
      } 

    }), (error => {this.errorMsg = error.error.message}));

  } 

}
