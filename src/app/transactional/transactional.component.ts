import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserAccountService } from '../service/data/user-account.service';

@Component({
  selector: 'app-transactional',
  templateUrl: './transactional.component.html',
  styleUrls: ['./transactional.component.css']
})
export class TransactionalComponent implements OnInit {
  client: any = {};
  transactionalAccounts: any = [];

  constructor(private route : Router, private service : UserAccountService) { }

  ngOnInit() {
    this.client = JSON.parse(sessionStorage.getItem('client'));
    let id = this.client.client_id;

    this.service.getTransactionaUserAccounts(id).subscribe(response => {
      this.transactionalAccounts = response;
      console.log(this.transactionalAccounts);
    })

  }

  gotoWithdraw(account_num){
    this.route.navigate(['withdraw', account_num]);
  }
}
