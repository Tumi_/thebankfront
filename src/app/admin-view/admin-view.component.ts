import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserAccountService } from '../service/data/user-account.service';

@Component({
  selector: 'app-admin-view',
  templateUrl: './admin-view.component.html',
  styleUrls: ['./admin-view.component.css']
})
export class AdminViewComponent implements OnInit {

  user: any = {};
  accounts: any = [];
  client_id: any;
  userId: any;
  highest: any;
  financial_position: any;
  noAccountsMsg;

  constructor(private route: Router, private service: UserAccountService, private activeRoute: ActivatedRoute) { }

  ngOnInit() {

    this.client_id = this.activeRoute.snapshot.params['client_id'];

    this.service.getUser(this.client_id).subscribe(response => {
      this.user = response

      this.userId = this.client_id;
      console.log(this.userId)

      this.service.getAllUserAccounts(this.userId).subscribe(response => {
        this.accounts = response;
        console.log(this.accounts);
      }, ((error => {this.noAccountsMsg = error.error.message})));

      this.service.getHighestAccBal(this.userId).subscribe(response => {
        this.highest = response;
        console.log(this.highest);
      });

      this.service.getFinancialPosition(this.userId).subscribe(response => {
        this.financial_position = response;
      });


    });

  }

  logout() {
    this.route.navigate(['']);
  }


}
