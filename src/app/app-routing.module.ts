import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { TransactionalComponent } from './transactional/transactional.component';
import { CurrencyComponent } from './currency/currency.component';
import { WithdrawComponent } from './withdraw/withdraw.component';
import { AdminComponent } from './admin/admin.component';
import { AdminViewComponent } from './admin-view/admin-view.component';

const routes: Routes = [
  {path: '', component:LoginComponent},
  {path: 'home', component:HomeComponent},
  {path: 'transactional', component:TransactionalComponent},
  {path: 'currency', component:CurrencyComponent},
  {path: 'withdraw/:account_num', component:WithdrawComponent},
  {path: 'admin', component:AdminComponent},
  {path: 'admin-view/:client_id', component:AdminViewComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
