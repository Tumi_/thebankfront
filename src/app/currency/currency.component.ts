import { Component, OnInit } from '@angular/core';
import { UserAccountService } from '../service/data/user-account.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-currency',
  templateUrl: './currency.component.html',
  styleUrls: ['./currency.component.css']
})
export class CurrencyComponent implements OnInit {
  client: any = {};
  currencyAccounts: any[];
  conversionRate: any;
  isConverted = false;
  zarAmnt;
  rate;

  constructor(private service: UserAccountService, private router : Router) { }
  currencyForm: FormGroup;

  ngOnInit() {
    this.client = JSON.parse(sessionStorage.getItem('client'));
    let id = this.client.client_id;
    // let curr;

    this.currencyForm = new FormGroup({
      'currencyControl': new FormControl(null, Validators.required)
    });

    this.service.getCurrencyUserAccounts(id).subscribe(response => {
      this.currencyAccounts = response;
      console.log(this.currencyAccounts)

      // for (let curr of this.currencyAccounts) {
      //   this.service.getConversionRate(curr.currency_code).subscribe(response => {
      //     this.conversionRate = response;

      //     console.log(this.conversionRate.rate)
      //   })
      // }
    });
  }

  goHome(){
    this.router.navigate(['home']);
  }

  handleConversion(currency_code, display_balance){
  
    this.service.getConversionRate(currency_code).subscribe(reponse => {
      this.conversionRate = reponse;

    this.rate = this.conversionRate.rate;

      this.service.convertCurrency(currency_code, display_balance, this.rate).subscribe(response => {
        this.zarAmnt = response;
        this.zarAmnt = this.zarAmnt.toFixed(2);
        this.isConverted = true;
    
      })
    })

  }
}
