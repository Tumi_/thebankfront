import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserAccountService } from '../service/data/user-account.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  isValid = false;
  message = "";
  // failedLoginCnt;
  client: any = {};

  constructor(private router: Router, private service: UserAccountService) { }
  loginForm: FormGroup;

  ngOnInit() {

    this.loginForm = new FormGroup({
      'name': new FormControl(null, Validators.required)
    })
  }

  handleLogin() {
    let name = this.loginForm.value.name;

    let client = { 'name': name };
    let resposeUser;

    if (name == 'admin'){
      this.router.navigate(['admin'])
    } else {
      this.service.login(client).subscribe((response => {
        resposeUser = response;
  
        if (resposeUser === null) {
          this.message = "User not found";
        } else {
          this.isValid = true;
          // console.log(resposeUser);
          sessionStorage.setItem('client', JSON.stringify(resposeUser));
          console.log(JSON.parse(sessionStorage.getItem('client')));
          this.router.navigate(['home']);
        }
      }));
    }
  }
}
