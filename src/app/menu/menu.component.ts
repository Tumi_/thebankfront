import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(private router : Router) { }

  ngOnInit() {
  }

  goHome(){
    this.router.navigate(['home']);
  }

  logout(){
    sessionStorage.clear();
    this.router.navigate(['']);
  }

}
