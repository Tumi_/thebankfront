import { Component, OnInit } from '@angular/core';
import { UserAccountService } from '../service/data/user-account.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  users : any = [];

  constructor(private service : UserAccountService, private route : Router) { }

  ngOnInit() {
    this.service.getAllUsers().subscribe(response => {
      this.users = response;
    })
  }

  logout(){
    this.route.navigate(['']);
  }

  viewAccounts(client_id){
    this.route.navigate(['admin-view', client_id]);
  }

}
