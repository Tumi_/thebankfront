import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_URL } from '../../app.constants';


@Injectable({
  providedIn: 'root'
})
export class UserAccountService {

  constructor(private http: HttpClient) { }
  //USER APIS

  login(user){
    return this.http.post<any>('http://localhost:8080/account/clients/login', user);
     // return this.http.post('http://localhost:8080/user/login', user, {responseType: 'text'});
  }

  getUser(client_id){
    return this.http.get<any>(`http://localhost:8080/account/clients/get-client/${client_id}`);
  }

  getTransactionaUserAccounts(client_id){
    return this.http.get<any>(`http://localhost:8080/account/transactional/${client_id}`);
  }

  getCurrencyUserAccounts(client_id){
    return this.http.get<any>(`http://localhost:8080/account/currency-accounts/${client_id}`);
  }

  getConversionRate(currency_code){
    return this.http.get<any>(`http://localhost:8080/account/currency-rate/${currency_code}`);
  }

  convertCurrency(currency_code,display_balance,rate){
    return this.http.get<any>(`http://localhost:8080/account/converted/${currency_code}/${display_balance}/${rate}`);
  }

  getAccount(account_num){
    return this.http.get<any>(`http://localhost:8080/account/get-account/${account_num}`);
  }

  withdraw(client_account_number, client_account){
    return this.http.put<any>(`http://localhost:8080/account/transactional/withdraw/${client_account_number}`,client_account);
  }

  getAllUsers(){
    return this.http.get<any>('http://localhost:8080/account/clients/all');
  }

  getAllUserAccounts(client_id){
    return this.http.get<any>(`http://localhost:8080/account/get-all-accounts/${client_id}`)
  }

  getHighestAccBal(client_id){
    return this.http.get<any>(`http://localhost:8080/account/highest/${client_id}`);
  }

  getFinancialPosition(client_id){
    return this.http.get<any>(`http://localhost:8080/account/financial-position/${client_id}`);
  }
}
